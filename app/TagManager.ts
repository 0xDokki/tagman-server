import {v1 as neo4j} from 'neo4j-driver';
import * as parser from 'parse-neo4j';
import path from 'path';
import {Router, Request, Response} from 'express';
import NetError from "./Util";

interface TagConfig {
    neo4j_url: string,
    neo4j_user: string,
    neo4j_pass: string,
}

class TagManager {
    private db_driver: neo4j.Driver;
    public Router: Router;

    constructor(config: TagConfig) {
        this.db_driver = neo4j.driver(config.neo4j_url,
            neo4j.auth.basic(config.neo4j_user, config.neo4j_pass));
        process.on('SIGTERM', () => {
            this.db_driver.close();
        });

        // create default tag group
        let session = this.db_driver.session();
        session.run('MERGE (tg:group {name: "misc"})')
            .then(() => session.close());

        this.Router = Router();
        this.Router.get('/files', this.r_ListFiles.bind(this));
        this.Router.get('/files/:file', this.r_GetFile.bind(this));

        this.Router.post('/tag', this.createWrapper('tag').bind(this));
        this.Router.delete('/tag', this.deleteWrapper('tag').bind(this));

        this.Router.post('/tagGroup', this.createWrapper('group').bind(this));
        this.Router.delete('/tagGroup', this.deleteWrapper('group').bind(this));

        this.Router.put('/group', this.linkWrapper('tag', 'group', 'in_group').bind(this));
        this.Router.get('/group', this.listLinkWrapper('tag', 'group', 'in_group').bind(this));
        this.Router.delete('/group', this.unlinkWrapper('tag', 'group', 'in_group').bind(this));

        this.Router.put('/link', this.linkWrapper('file', 'tag', 'tagged_by').bind(this));
        this.Router.get('/link', this.listLinkWrapper('file', 'tag', 'tagged_by').bind(this));
        this.Router.delete('/link', this.unlinkWrapper('file', 'tag', 'tagged_by').bind(this));
    }

    public pushInitialFiles(files: string[]): void {
        let session = this.db_driver.session();
        let tx = session.beginTransaction();

        files.forEach((file: string) => {
            tx.run('MERGE (f:file {name: {filename} })' +
                'SET f.last_seen = {path}',
                {filename: path.basename(file), path: file});
        });

        tx.commit().catch((err: Error) => {
            console.error(err);
            process.abort();
        }).then(() => {
            console.info('Initial file commit done.');
        })
    }

    public updateFile(file: string, deleted: boolean = false): void {
        let session = this.db_driver.session();
        let name = path.basename(file);

        session.run('MERGE (f:file {name: {filename} })' +
            'SET f.last_seen = {path}',
            {filename: name, path: deleted ? '' : file});
        session.close();
    }

    public r_ListFiles(req: Request, resp: Response): void {
        let query = 'MATCH (f:file) RETURN f';

        let session = this.db_driver.session();
        session.run(query)
            .then(parser.parse)
            .then(res => resp.json(res))
            .then(() => session.close());
    }

    public r_GetFile(req: Request, resp: Response): void {
        let file = req.params['file'];

        let query = 'MATCH (f:file { name: {fileName} }) RETURN f';

        let session = this.db_driver.session();
        session.run(query, {fileName: file})
            .then(parser.parse)
            .then(async (res: any) => {
                let tags: object[] = await session.run(
                    'MATCH (file {name: {name} })-[:tagged_by]->(t)-[:in_group]->(g) RETURN t,g',
                    {name: file},
                ).then(parser.parse);
                // next two calls convert tag-group pairs to group->tag[] mapping
                tags = tags.map((itm: any) => ({tag: itm['t'].name, group: itm['g'].name}));
                tags = tags.reduce((prev: any, curr: any) => {
                    if (!(curr.group in prev)) prev[curr.group] = [];
                    prev[curr.group].push(curr.tag);
                    return prev;
                }, {});

                res = {
                    name: res[0].name,
                    last_seen: res[0].last_seen,
                    tags: tags,
                };
                resp.json(res);
            })
            .then(() => session.close());
    }

    private createWrapper(name: string): (req: Request, resp: Response) => void {
        return (req: Request, resp: Response) => {
            if (!(req.body) || !(name in req.body)) {
                resp.status(400).json(new NetError(400, 'missing parameters'));
                return;
            }
            let val = req.body[name];

            let session = this.db_driver.session();
            session.run(`MERGE (v:${name} {name: {val} }) RETURN v`, {val: val})
                .then(res => {
                    if (res.records.length === 0) throw new NetError(500, `Error while creating ${name}`);
                    resp.status(201).json(res.records[0].get('v'));
                })
                .catch((err: NetError) => resp.status(err.code).json(err))
                .finally(() => session.close());
        }
    }

    private deleteWrapper(name: string): (req: Request, resp: Response) => void {
        return (req: Request, resp: Response) => {
            if (!(req.body) || !(name in req.body)) {
                resp.status(400).json(new NetError(400, 'missing parameters'));
                return;
            }
            let val = req.body[name];

            let session = this.db_driver.session();
            session.run(`MATCH (v:${name} {name: {val} }) RETURN v`,
                {val: val})
                .then(res => {
                    if (res.records.length === 0) throw new NetError(404, `${name} not found.`);
                })
                .then(() =>
                    session.run(`MATCH (v:${name} {name: {val} }) DELETE v`,
                        {val: val}))
                .then(() => resp.status(204).json({}))
                .catch((err: NetError) => resp.status(err.code).json(err))
                .finally(() => session.close());
        }
    }

    private linkWrapper(source: string, target: string, link: string): (req: Request, resp: Response) => void {
        return (req: Request, resp: Response) => {
            if (!(req.body) || !(source in req.body) || !(target in req.body)) {
                resp.status(400).json(new NetError(400, 'missing parameters'));
                return;
            }
            let sourceValue = req.body[source];
            let targetValue = req.body[target];

            let session = this.db_driver.session();
            session.run(`MATCH (s:${source} {name: {src} }),(t:${target} {name: {tgt} }) RETURN s,t`,
                {src: sourceValue, tgt: targetValue})
                .then(res => {
                    if (res.records.length === 0)
                        throw new NetError(404, `${source} or ${target} not found.`);
                })
                .then(() =>
                    session.run(`MATCH (s:${source} {name: {src} }),(t:${target} {name: {tgt} })`
                        + `MERGE (s)-[r:${link}]->(t) RETURN r`,
                        {src: sourceValue, tgt: targetValue}))
                .then(res => {
                    if (res.records.length === 0) throw new NetError(500, 'Error while adding link.');
                    resp.status(201).json(res.records[0].get('r'));
                })
                .catch((err: NetError) => resp.status(err.code).json(err))
                .finally(() => session.close());
        };
    }

    private listLinkWrapper(source: string, target: string, link: string): (req: Request, resp: Response) => void {
        return (req: Request, resp: Response) => {
            if (!(req.query) || !((source in req.query)).xor((target in req.query))) {
                resp.status(400).json(new NetError(400, 'missing or illegal parameter'));
                return;
            }
            let sourceValue = req.query[source];
            let targetValue = req.query[target];

            let query = '';
            if (sourceValue) query = `MATCH (:${source} {name: {val} })-[:${link}]->(t:${target}) RETURN t`;
            else if (targetValue) query = `MATCH (s:${source})-[:${link}]->(:${target} {name: {val} }) RETURN s`;

            let session = this.db_driver.session();
            session.run(query, {val: sourceValue || targetValue})
                .then(parser.parse)
                .then(res => resp.json(res))
                .then(() => session.close());
        }
    }

    private unlinkWrapper(source: string, target: string, link: string): (req: Request, resp: Response) => void {
        return (req: Request, resp: Response) => {
            if (!(req.body) || !(source in req.body) || !(target in req.body)) {
                resp.status(400).json(new NetError(400, 'missing parameters'));
                return;
            }
            let sourceValue = req.body[source];
            let targetValue = req.body[target];

            let session = this.db_driver.session();
            session.run(`MATCH (s:${source} {name: {src} }),(t:${target} {name: {tgt} }) RETURN s,t`,
                {src: sourceValue, tgt: targetValue})
                .then(res => {
                    if (res.records.length === 0)
                        throw new NetError(404, `${source} or ${target} not found.`);
                })
                .then(() =>
                    session.run(`MATCH (s:${source} {name: {src} }),(t:${target} {name: {tgt} })` +
                        `MERGE (s)-[r:${link}]->(t) DELETE r`,
                        {src: sourceValue, tgt: targetValue}))
                .then(res => {
                    if (res.records.length !== 0) throw new NetError(500, 'Error while removing link.');
                    resp.status(204).json({});
                })
                .catch((err: NetError) => resp.status(err.code).json(err))
                .finally(() => session.close());
        }
    }
}

export = TagManager;
