interface Boolean {
    xor: (other: boolean) => boolean
}
Boolean.prototype.xor = function (other: boolean): boolean {
    return (this || other) && !(this && other);
};