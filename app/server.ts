import express from 'express';
import fs from 'fs';
import cors from 'cors';
import './proto';
import TagManager from './TagManager';
import FileManager from './FileManager';

const config = JSON.parse(fs.readFileSync('./config/config.json').toString());

const tags: TagManager = new TagManager(config.tag);
const files: FileManager = new FileManager(config.fs, tags);

const app: express.Application = express();
app.use(cors({
    origin: 'http://localhost:3000',
    optionsSuccessStatus: 200
}));
app.use(express.json());
app.use('/f', files.Router);
app.use('/t', tags.Router);

app.listen(3333, () => {
    console.info("backend online.")
});
